import os
import glob
import time
import subprocess
import shutil

from meli3d import vina_dock
from meli3d import interaction_similarity

from rdkit import Chem
from rdkit.Chem import AllChem

MGLTools_DIR = '/data/user_home/wdjang/anaconda3/pkgs/mgltools-1.5.6-1/'
MGLToolsPkg_DIR = MGLTools_DIR + 'MGLToolsPckgs/AutoDockTools/Utilities24/'
MGLTools_PYTHON_DIR = MGLTools_DIR + 'bin/pythonsh'
PLIP_path = './bin/plip-stable/plip/plipcmd'

###########################################
## AutoDock Vina and Modeller parameters ##
###########################################
vina_mode_num = 10
vina_energy_range = 4
vina_exhaustiveness = 8
vina_cpu_num = 4
vina_grid_point = 14

### conformer
conf_dir = "../preprocess/conformer"

### pre-docking results
shape_sim_res = "../sample_inputs/docking_input.txt"

### docking output folder
docking_results = "./docking_results_20201122"
try:
    os.mkdir("./docking_results_20201122")
except:
    pass


def get_ligand(shape_sim_res):
    with open(shape_sim_res, "r") as fr:
        lines = fr.read().splitlines()
        for li in lines:
            print(li)
            cid = li.split('\t')[0]
            shape_sim =  li.split('\t')[1]
            q_mol = cid.split('*')[0]
            t_mol = cid.split('*')[1]
            t_filename = '_'.join(t_mol.split('_')[0:5])
            conf_num = int(t_mol.split('_')[5])
            print(t_filename)
            docking_output_dir = "%s/%s_%s/" %(docking_results, t_filename, conf_num)
            try:
                os.mkdir(docking_output_dir)
            except:
                print("Exists docking file")
                continue

            plip_output_dir = docking_output_dir+'/PLIP_results/'
            
            try:
                os.mkdir(plip_output_dir)
            except:
                shutil.rmtree(plip_output_dir)
                os.mkdir(plip_output_dir)


            conf_file = "%s/%s.sdf" %(conf_dir, t_filename)
            if os.path.exists(conf_file):
                
                suppl = Chem.SDMolSupplier(conf_file)
                mols = [x for x in suppl]
                sel_mol = mols[conf_num-1]
                res = AllChem.MMFFOptimizeMoleculeConfs(sel_mol)
                print(res)
                lig_file_name = docking_output_dir+'/'+t_filename+'.pdb'
                AllChem.MolToPDBFile(sel_mol, lig_file_name)
                
                
                pdb_id = "./6y2f_refine_em.pdb"
                try:
                    docking(pdb_id, t_filename, shape_sim, docking_output_dir, lig_file_name)
                    fp_results = open(plip_output_dir+'/Interaction_similarity.txt', 'a')
                    interaction_sim_info = interaction_sim(q_mol, pdb_id, plip_output_dir, docking_output_dir, t_filename)
                    for k in interaction_sim_info.keys():
                        fp_results.write(str(k) + '\t' + str(interaction_sim_info[k]) + '\n')
                    fp_results.close()
                except:
                print("No conformer files")
    return

def docking(pdb_id, t_filename, shape_sim, output_dir, lig_sdf_file):
    pdb_id = "./6y2f_refine_em.pdb"
    nat_lig_file = "./6y2f_ligand.pdb"

    vina_dock.run_vina_docking(t_filename, pdb_id, lig_sdf_file, nat_lig_file, output_dir, MGLToolsPkg_DIR, MGLTools_PYTHON_DIR, vina_mode_num, vina_energy_range, vina_exhaustiveness, vina_cpu_num, vina_grid_point)
    return

def interaction_sim(q_mol, pdb_id, plip_output_dir, docking_output_dir, t_filename):
    docking_pdbqt_file = docking_output_dir+'/Docking_result_%s.pdbqt' %t_filename
    if os.path.exists(docking_pdbqt_file):
        natural_ligand_file = "./plip_ligand/%s.pdb" %q_mol  ## 6y2f bound to active compounds
        #natural_ligand_file = "./plip_ligand_rdrp/%s.pdb" %q_mol
        print(t_filename, natural_ligand_file)
        cryst_plip_folder = interaction_similarity.run_PLIP_crystal(natural_ligand_file, plip_output_dir, PLIP_path)
        docking_plip_folder = interaction_similarity.run_PLIP_pdbqt(pdb_id, docking_pdbqt_file, plip_output_dir, t_filename, PLIP_path)
        interaction_sim_info = interaction_similarity.calculate_interaction_similarity(pdb_id, cryst_plip_folder, docking_plip_folder)
    return interaction_sim_info


if __name__ == "__main__":

    cpu_start = time.clock()
    
    get_ligand(shape_sim_res)

    cpu_end = time.clock()

    print("cpu_time: ", cpu_end - cpu_start)



