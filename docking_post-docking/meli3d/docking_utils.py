import subprocess
import os
import glob

def read_grid(filename, grid_point):
    grid_info = {}
    fp = open(filename, 'r')
    grid_info['x_size'] = grid_point
    grid_info['y_size'] = grid_point
    grid_info['z_size'] = grid_point
    for line in fp:
        if 'gridcenter' in line:
            sptlist = line.strip().split(' ')
            x_center = float(sptlist[1].strip())
            y_center = float(sptlist[2].strip())
            z_center = float(sptlist[3].strip())
            grid_info['x_center'] = x_center
            grid_info['y_center'] = y_center
            grid_info['z_center'] = z_center
    fp.close()
    return grid_info

def convert_smiles_to_molecule(smiles, output_file, output_type):
    print ('molconvert %s -3 -s %s -o %s' % (output_type, smiles, output_file))
    subprocess.call('molconvert %s -3 -s %s -o %s' % (output_type, smiles, output_file), shell=True)
    return

def make_mol_from_smi(ligand_file, output_file):
    #subprocess.call("babel --gen3d -ismi %s -O %s -p 7.2" % (ligand_file, output_file), shell=True)
    subprocess.call("babel -isdf %s -O %s -p 7.2" % (ligand_file, output_file), shell=True)

def ligand_energy_optimization(output_type, input_file, output_file):
    print ("obminimize -o %s %s > %s" % (output_type, input_file, output_file))
    subprocess.call("obminimize -o %s %s > %s" % (output_type, input_file, output_file), shell=True)
    return

def make_vina_conf_file(ligand_info, output_config_file, exhaustiveness=8, num_mode=10, cpu=4):
    fp = open(output_config_file, 'w')
    
    fp.write('center_x = %s\n' % (ligand_info['x_center']))
    fp.write('center_y = %s\n' % (ligand_info['y_center']))
    fp.write('center_z = %s\n' % (ligand_info['z_center']))
    
    fp.write('size_x = %s\n' % (ligand_info['x_size']))
    fp.write('size_y = %s\n' % (ligand_info['y_size']))
    fp.write('size_z = %s\n' % (ligand_info['z_size']))
    
    fp.write('exhaustiveness = %s\n' % (exhaustiveness))
    fp.write('num_modes = %s\n' % (num_mode))
    
    fp.write('cpu = %s\n' % (cpu))

    fp.close()
    return

def prepare_ligand_with_autodock_tools(python_dir, python_module_file, ligand_file, output_file, isRigid=False):

    if isRigid == False:
        subprocess.call(python_dir + " " + python_module_file + "prepare_ligand4.py  -l %s -o %s" % (ligand_file, output_file),
            shell=True)
    else:
        subprocess.call(python_dir + " " + python_module_file + "prepare_ligand4.py -Z -l %s -o %s" % (ligand_file, output_file),
            shell=True)

def prepare_receptor_with_autodock_tools(python_dir, python_module_file, receptor_file, output_file):
    subprocess.call(python_dir + " " + python_module_file + "prepare_receptor4.py -r %s -o %s" % (
            receptor_file, output_file),shell=True)

def calculate_grid_box(python_dir, python_module_file, ligandfile, receptorfile, outputfile,
                       npts=[10.0, 10.0, 10.0]):
    gird_info = {}
    npts_str_list = [str(each_p) for each_p in npts]
    npts_str = ','.join(npts_str_list)

    subprocess.call(
    python_dir + " " + python_module_file + 'prepare_gpf.py -l %s -r %s -o %s -p npts="%s"' % (
        ligandfile, receptorfile, outputfile, npts_str),
    shell=True)
    return gird_info

def trim_pdb_file(receptor_file, receptor_output_file):
    fp = open(receptor_file, 'r')
    fp2 = open(receptor_output_file, 'w')
    for line in fp:
        if 'ATOM' in line:
            fp2.write('%s\n'%(line.strip()))
    fp2.write('TER\n')
    fp.close()
    fp2.close()

def vina_docking(receptor_file, ligand_file, conf_file, output_file, log_file, mode_num, energy_range, exhaustiveness, cpu_num):
    subprocess.call(
        ['vina', '--receptor', receptor_file, '--ligand', ligand_file, '--out', output_file, '--log', log_file,
         '--num_modes', str(mode_num),
         '--config', conf_file, '--energy_range', str(energy_range), '--exhaustiveness', str(exhaustiveness),
         '--cpu', str(cpu_num)])

