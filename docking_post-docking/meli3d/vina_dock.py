import subprocess
import os
import glob
from .docking_utils import *


def run_vina_docking(file_id, pdb_id, lig_file, natural_lig_file, output_dir, MGLTools_DIR, MGL_python_DIR, mode_num, energy_range, exhaustiveness, cpu_num, grid_point):
    ligand_file = lig_file
    #with open(ligand_file, 'w') as fp:
    #    fp.write('%s'%(smiles))

    stable_output_sdf = output_dir+'/%s.pdb'%(file_id)

    #make_mol_from_smi(ligand_file, output_file)

    #new_input_file = output_file
    #stable_output_sdf = output_dir+'/%s_optimized.pdb'%(file_id)
    #ligand_energy_optimization('pdb', new_input_file, stable_output_sdf)

    output_file = output_dir+'/%s.pdbqt'%(file_id)
    prepare_ligand_with_autodock_tools(MGL_python_DIR, MGLTools_DIR, stable_output_sdf, output_file, isRigid=False)

    #ligand_file, receptor_file, prot_chain = search_db_ligand_file(pdb_id, BIOLIP_DIR)

    ## prepare .mol file of natural ligand
    new_ligand_output_file = output_dir+'/natural_ligand_%s.pdbqt'%(file_id)
    prepare_ligand_with_autodock_tools(MGL_python_DIR, MGLTools_DIR, natural_lig_file, new_ligand_output_file, isRigid=False)

    receptor_file = pdb_id
    receptor_output = output_dir+'/Receptor_%s.pdbqt'%(file_id)
    prepare_receptor_with_autodock_tools(MGL_python_DIR, MGLTools_DIR, receptor_file, receptor_output)
    if os.path.exists(receptor_output):
        receptor_output_file = output_dir+'/Receptor_%s_trim.pdbqt'%(file_id)
        trim_pdb_file(receptor_output, receptor_output_file)

        output_grid_file = output_dir+'/%s_grid.gpf'%(file_id)
        calculate_grid_box(MGL_python_DIR, MGLTools_DIR, new_ligand_output_file, receptor_output_file, output_grid_file, [grid_point, grid_point, grid_point])

        ## binding grid confirm
        grid_info = read_grid(output_grid_file, grid_point)

        conf_grid_output_file = output_dir+'/config_%s_grid.txt'%(file_id)
        make_vina_conf_file(grid_info, conf_grid_output_file)

        ligand_docking_file = output_file
        receptor_docking_file = receptor_output_file
        config_docking_file = conf_grid_output_file
        output_file = output_dir+'/Docking_result_%s.pdbqt'%(file_id)
        log_file = output_dir+'/Docking_log_%s.log'%(file_id)

        vina_docking(receptor_docking_file, ligand_docking_file, config_docking_file, output_file, log_file, mode_num, energy_range, exhaustiveness, cpu_num)

    return


