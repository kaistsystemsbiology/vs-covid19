from molvs import Standardizer
from rdkit import Chem
from rdkit.Chem import SaltRemover, AllChem, Descriptors

import os, time

### Database ID, SMILES
cnt = 0
cmpd_collect = {}
drugbank_id_list =[]
chembl_id_list =[]


def Gparsing_drugbank_sdf(sdf_fileName):
    with open(sdf_fileName) as fr:
        global cnt
        read_db_id = False
        read_smi = False
        mol_id = ""
        mol_smi = ""
        lines = fr.read().splitlines()
        for li in lines:
            if "$$$$" in li:
                if mol_id and mol_smi and mol_id not in drugbank_id_list:
                    cnt+=1
                    unique_id = "MBEL_CORONA_00000%s" %cnt
                    cmpd_collect[unique_id] = {"DB_ID":mol_id, "SMILES":mol_smi}
                    drugbank_id_list.append(mol_id)
                    mol_id = ""
                    mol_smi = ""
            elif read_db_id:
                mol_id = li.strip()
                read_db_id = False
            elif read_smi:
                mol_smi = li.strip()
                read_smi = False
            elif "> <DATABASE_ID>" in li:
                read_db_id = True
            elif "> <SMILES>" in li:
                read_smi = True
    print("Number of cumulative compoudns for DrugBank drugs: %s" %cnt)
    return


def parsing_zinc_sdf(sdf_fileName):
    with open(sdf_fileName) as fr:
        global cnt
        read_db_id = False
        read_smi = False
        mol_id = ""
        mol_smi = ""
        lines = fr.read().splitlines()
        for li in lines:
            if "$$$$" in li:
                if mol_id and mol_smi:
                    cnt+=1
                    unique_id = "MBEL_CORONA_00000%s" %cnt
                    cmpd_collect[unique_id] = {"DB_ID":mol_id, "SMILES":mol_smi}
                    mol_id = ""
                    mol_smi = ""
            elif read_db_id:
                mol_id = li.strip()
                read_db_id = False
            elif read_smi:
                mol_smi = li.strip()
                read_smi = False
            elif ">  <zinc_id>" in li:
                read_db_id = True
            elif ">  <smiles>" in li:
                read_smi = True
    print("Number of cumulative compoudns for ZINC drugs: %s" %cnt)
    return


def parsing_chembl_tsv(tsv_fileName):
    with open(tsv_fileName) as fr:
        global cnt
        mol_id = ""
        mol_smi = ""
        lines = fr.read().splitlines()
        for li in lines[1:]:
            tok = li.split('\t')
            mol_id = tok[0]
            mol_id = mol_id.split('"')[1]
            mol_smi = tok[7]
            mol_smi = mol_smi.split('"')[1]
            if mol_id and mol_smi and mol_id not in chembl_id_list:
                cnt+=1
                unique_id = "MBEL_CORONA_00000%s" %cnt
                cmpd_collect[unique_id] = {"DB_ID":mol_id, "SMILES":mol_smi}
                chembl_id_list.append(mol_id)
                mol_id = ""
                mol_smi = ""
    print("Number of cumulative compoudns for ChEMBL drugs: %s" %cnt)
    return


def standardization_by_molvs():
    del_list = []
    remover = SaltRemover.SaltRemover()
    for unique_id, cmpd_info in cmpd_collect.items():
        try:
            mol = Chem.MolFromSmiles(cmpd_info["SMILES"])
            s = Standardizer()
            smol = s.standardize(mol)
            res = remover.StripMol(smol)
            smi = Chem.MolToSmiles(res)
            inchi = Chem.MolToInchi(res)
            cmpd_info["SMILES"] = smi
            cmpd_info["InChI"] = inchi
            cmpd_collect[unique_id] = cmpd_info
        except:
            print("This compound makes error: " + cmpd_info["SMILES"])
            del_list.append(unique_id)

    for dl in del_list:
        del cmpd_collect[dl]
    return


def remove_redundant():
    inchi_list = []
    del_list = []
    for unique_id, cmpd_info in cmpd_collect.items():
        if cmpd_info["InChI"] not in inchi_list:
            inchi_list.append(cmpd_info["InChI"])
        else:
            del_list.append(unique_id)

    for dl in del_list:
        del cmpd_collect[dl]

    with open("final_cmpd_library.txt", "w") as fw:
        for unique_id, cmpd_info in cmpd_collect.items():
            fw.write("%s\t%s\t%s\t%s\n" %(unique_id, cmpd_info["DB_ID"], cmpd_info["SMILES"], cmpd_info["InChI"]))
    return


def conformer_generation():
    cnt = 0
    with open('../sample_inputs/cmpd_input_one.txt') as fr:
        lines = fr.read().splitlines()
        for li in lines:
            tok = li.split('\t')
            unique_id = tok[0]
            smi = tok[2]
            try:
                m = Chem.MolFromSmiles(smi)
                m2 = Chem.AddHs(m)
                mw = Descriptors.ExactMolWt(m)
                print("cid: ", unique_id)
                print("MW: ", mw)
                if mw > 1000:
                    continue

                # run ETKDG 300 times
                cids = AllChem.EmbedMultipleConfs(m2, numConfs=300, numThreads=1)

                nMol = len(cids)
                print("num of conformers: ", nMol)
                try:
                    os.mkdir("./conformer")
                except:
                    pass
                w = Chem.SDWriter('./conformer/conf_output_'+unique_id+'.sdf')
                for prbNum in range(0, nMol):
                    prbMol = cids[prbNum]
                    w.write(m2, prbMol)
                w.close()
                print('Success: cnt %s %s' %(cnt, unique_id))
                cnt += 1
            except:
                print('error')
                pass
    return


if __name__ == "__main__":

    cpu_start = time.clock()
    conformer_generation()
    cpu_end = time.clock()
    print("cpu time: ", cpu_end - cpu_start)
