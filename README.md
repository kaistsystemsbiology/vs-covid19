# Virtual screening for COVID-19 (vs-covid19) #
Docking-based virtual screening with pre- and post-docking pharmacophore filtering

### How to install
---
### Requirements:
An Anaconda python environment is required for easy installation of third-party programs.
```
- python version 3
- autodock-vina
- mgltoos
- rdkit
- openbabel
```
It works on Linux and has been tested on Ubuntu 16.04.
### vs-covid19
```
1. Clone the repository
git clone https://wdjang@bitbucket.org/kaistsystemsbiology/vs-covid19.git
```

```
2. Create and activate a conda environment
conda env create -f environment.yml
conda activate vs-covid19
```
---
Initial settings for third-party programs (MGLTools and PLIP)

```
### MGLTools
1. Edit /[YOUR_HOME_PATH]/anaconda3/pkgs/mgltools-1.5.6-1/bin/pythonsh (in line 9)
Replace MGL_ROOT="/opt/anaconda1anaconda2anaconda3" with **MGL_ROOT="[YOUR_HOME_PATH]/anaconda3/pkgs/mgltools-1.5.6-1/"**
2. Edit run_docking.py (in line 13)
Replace MGLTools_DIR="/XXXX/anaconda3/pkgs/mgltools-1.5.6-1/" with **MGLTools_DIR="/[YOUR_HOME_PATH]/anaconda3/pkgs/mgltools-1.5.6-1/"**

### PLIP 
To use the plip packages for interaction analysis,
1. cd /vs-covid19/docking_post-docking/bin/plip-stable
2. python setup.py install

```

---
### Publication

