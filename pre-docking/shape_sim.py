import os
import glob
import time
import numpy as np
from numpy.testing import assert_array_almost_equal, assert_almost_equal

from rdkit.Chem import rdmolfiles
import oddt
from oddt.shape import usr, usr_cat, electroshape, usr_similarity


def merge_results(res_dir):
    res_tot = {}
    dir_files = glob.glob(res_dir+'/*')
    for df in dir_files:
        with open(df) as fr:
            lines = fr.read().splitlines()
            for li in lines:
                tok = li.split("\t")
                cmpd_id = tok[0]
                usr = float(tok[1])
                usr_cat = float(tok[2])
                electro = float(tok[3])
                avg = (usr + usr_cat + electro) / 3
                res_tot[cmpd_id] = avg
    return res_tot

def calculate_shape_sim(q_file, t_file):
    results = {}
    q_mol_name = os.path.basename(q_file).split('.')[0]
    t_mol_name = os.path.basename(t_file).split('.')[0]
    
    q_sdf_mol = next(oddt.toolkit.readfile('sdf', q_file))
    t_sdf_mols = list(oddt.toolkit.readfile('sdf', t_file))
   
    cnt = 1
    usr_query_shape = usr(q_sdf_mol)
    usr_cat_query_shape = usr_cat(q_sdf_mol)
    electroshape_query_shape = electroshape(q_sdf_mol)
    for t_mol in t_sdf_mols:
        try:
            usr_mol_shape = usr(t_mol)
            usr_cat_mol_shape = usr_cat(t_mol)
            electroshape_mol_shape = electroshape(t_mol)

            usr_sim = usr_similarity(usr_query_shape, usr_mol_shape)
            usr_cat_similarity = usr_similarity(usr_cat_query_shape, usr_cat_mol_shape)
            electroshape_similarity = usr_similarity(electroshape_query_shape, electroshape_mol_shape)

            results[q_mol_name+'*'+t_mol_name+'_'+str(cnt)] = {"usr":usr_sim, "usr_cat":usr_cat_similarity, "electroshape":electroshape_similarity}
            cnt+=1
            #print(q_mol_name, t_mol_name, usr_sim, usr_cat_similarity, electroshape_similarity)
        except:
            with open('./error_log.txt', 'a') as fw:
                fw.write(q_mol_name + '\t' + t_mol_name + '\t' + str(cnt) + '\n')
    return results


### Calculate shape similarity
start = time.clock()
#query_dirpath = ["./active_cmpd/Mpro-x1077_0.sdf", "./active_cmpd/Mpro-x1093_0.sdf", "./active_cmpd/Mpro-x1249_0.sdf"]
query_dirpath = glob.glob("./active_cmpd/*")
try:
    os.mkdir("./shape_sim_results")
except:
    pass
target_dirpath = glob.glob("../preprocess/conformer/*")
for query_file in query_dirpath:
    q_mol_name = os.path.basename(query_file).split('.')[0]
    for target_file in target_dirpath:
        print(target_file)
        results = calculate_shape_sim(query_file, target_file)
        with open('./shape_sim/'+q_mol_name+'.txt', 'a') as fw:
            for name, shape_info in results.items():
                 fw.write("%s\t%s\t%s\t%s\n" %(name, shape_info["usr"], shape_info["usr_cat"], shape_info["electroshape"]))

### Merging the results of shape similarity
shape_sim_cutoff = 0.4
result_dir = "./shape_sim_results"
res_tot_dict = merge_results(result_dir)
sdict = sorted(res_tot_dict.items(), key=lambda x: x[1], reverse=True)

with open("./shape_sim_sorted.txt", "w") as fw:
    for s in sdict:
        if s[1] < shape_sim_cutoff:
            continue
        else:
            fw.write("%s\t%s\n" %(s[0], s[1]))

end = time.clock()
print("cpu time: ", end - start)
